//
//  Typealias.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 9/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public typealias ItemsSuccessClosure<T> = (T) -> Void

