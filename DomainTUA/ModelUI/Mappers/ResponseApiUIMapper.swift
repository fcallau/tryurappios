//
//  ResponseApiUIMapper.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 19/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public func mapResponse(from responseAPIUI: ResponseApiUI) -> ResponseTUA {
    var response = ResponseTUA()
    
    response.status = responseAPIUI.status
    response.resultDescription = responseAPIUI.resultDescription
    if (responseAPIUI.result != nil) { response.result = mapUser(from: responseAPIUI.result!) } else { response.result = nil }
    
    return response
}

public func mapResponse(from responseTUA: ResponseTUA) -> ResponseApiUI {
    var response = ResponseApiUI(status: 0)
    
    response.status = responseTUA.status
    response.resultDescription = responseTUA.resultDescription
    if (responseTUA.result != nil) { response.result = mapUser(from: responseTUA.result!) } else { response.result = nil }
    
    return response
}
