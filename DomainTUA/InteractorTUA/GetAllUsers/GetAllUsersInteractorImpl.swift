//
//  GetAllUsersInteractorImpl.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 9/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public class GetAllUsersInteractorImpl: GetAllItems {
    
    public typealias ItemsType = UsersUI
    
    public init() {}
    
    public func execute(success: @escaping (ResponseUsersTUA) -> Void, onError: @escaping (Error) -> Void) {
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        
        repository.getAllUsers(success: { (response) in
            success(response)
        }) { (error) in
            onError(error)
        }
    }
}
