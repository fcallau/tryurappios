//
//  GetAllItemsInteractor.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 9/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public protocol GetAllItems {
    func execute(success: @escaping ResponseUsersSuccessClosure, onError: @escaping ErrorClosure)
}


