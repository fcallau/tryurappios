//
//  GetBusinessesByUserEmailInteractorImpl.swift
//  TryUrApp
//
//  Created by Francesc Callau Brull on 11/9/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public class GetBusinessesByUserEmailInteractorImpl: GetBusinessesByUserEmailInteractor {
    
    public typealias ItemsType = BusinessesUI
    
    public init() {}
    
    
//    public typealias ErrorClosure = (Error) -> Void
//    public typealias UserSuccessClosure = (UserTUA) -> Void
//    public typealias ResponseSuccessClosure = (ResponseTUA) -> Void
//    public typealias ResponseUsersSuccessClosure = (ResponseUsersTUA) -> Void
//    public typealias ResponseBusinessSuccessClosure = (ResponseBusinessTUA) -> Void
//    public typealias ResponseBusinessesSuccessClosure = (ResponseBusinessesTUA) -> Void
    // func execute(user: UserUI, success: @escaping ResponseBusinessesSuccessClosure, onError: @escaping ErrorClosure)
    public func execute(user: UserUI, success: @escaping (ResponseBusinessesTUA) -> Void, onError: @escaping (Error) -> Void) {
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        let userTUA = mapUser(from: user)
        
        repository.getBusinessesByUserEmail(user: userTUA, success: { (response) in
            success(response)
        }, errorClosure: { (error) in
            onError(error)
        })
    }
}
