//
//  UploadImageInteractorImpl.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 26/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public class UploadImageInteractorImpl: UploadImageInteractor {
    
    public typealias ItemsType = BusinessUI
    
    public init() {}
    
    public func execute(business: BusinessUI, user: UserUI, imageType: String, image: String, success: @escaping (ResponseBusinessTUA) -> Void, onError: @escaping ErrorClosure) {
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        let userTUA = mapUser(from: user)
        let businessTUA = mapBusiness(from: business)
        repository.uploadImage(business: businessTUA, user: userTUA, imageType: imageType, image: image, success: { (response) in
            success(response)
        }) { (error) in
            onError(error)
        }
    }
}
