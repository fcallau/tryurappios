//
//  UpdateUserInteractorImpl.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 20/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public class UpdateUserInteractorImpl: UpdateUserInteractor {
    
    public typealias ItemsType = UserUI
    
    public init() {}
    
    public func execute(user: UserUI, success: @escaping (ResponseTUA) -> Void, onError: @escaping ErrorClosure) {
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        let userTUA = mapUser(from: user)
        repository.updateUser(user: userTUA, success: { (response) in
            success(response)
        }) { (error) in
            onError(error)
        }
    }
}
