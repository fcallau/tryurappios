//
//  DeleteBusinessInteractor.swift
//  TryUrApp
//
//  Created by Francesc Callau Brull on 16/9/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public protocol DeleteBusinessInteractor {
    func execute(business: BusinessUI, user: UserUI, success: @escaping ResponseBusinessSuccessClosure, onError: @escaping ErrorClosure)
}
