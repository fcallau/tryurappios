//
//  UpdateUserInteractor.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 14/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public protocol UpdateUserInteractor {
    func execute(user: UserUI, success: @escaping ResponseSuccessClosure, onError: @escaping ErrorClosure)
}
