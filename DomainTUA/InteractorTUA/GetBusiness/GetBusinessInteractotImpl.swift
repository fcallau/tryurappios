//
//  GetBusinessInteractotImpl.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 25/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public class GetBusinessInteractorImpl: GetBusinessInteractor {
    
    public typealias ItemsType = BusinessUI
    
    public init() {}
    
    public func execute(business: BusinessUI, user: UserUI, success: @escaping (ResponseBusinessTUA) -> Void, onError: @escaping ErrorClosure) {
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        let userTUA = mapUser(from: user)
        let businessTUA = mapBusiness(from: business)
        repository.getBusiness(business: businessTUA, user: userTUA, success: { (response) in
            success(response)
        }) { (error) in
            onError(error)
        }
    }
}
