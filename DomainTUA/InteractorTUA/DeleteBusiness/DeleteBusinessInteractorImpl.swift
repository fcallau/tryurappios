//
//  DeleteBusinessInteractorImpl.swift
//  TryUrApp
//
//  Created by Francesc Callau Brull on 16/9/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public class DeleteBusinessInteractorImpl: DeleteBusinessInteractor {
    
    public typealias ItemsType = BusinessUI
    
    public init() {}
    
    public func execute(business: BusinessUI, user: UserUI, success: @escaping (ResponseBusinessTUA) -> Void, onError: @escaping ErrorClosure) {
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        let businessTUA = mapBusiness(from: business)
        let userTUA = mapUser(from: user)
        repository.deleteBusiness(business: businessTUA, user: userTUA, success: { (response) in
            success(response)
        }) { (error) in
            onError(error)
        }
    }
}
