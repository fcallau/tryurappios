//
//  GetAllBusinessesInteractorImpl.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 25/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public class GetAllBusinessesInteractorImpl: GetAllBusinessInteractor {
    
    public typealias ItemsType = BusinessesUI
    
    public init() {}
    
    public func execute(success: @escaping (ResponseBusinessesTUA) -> Void, onError: @escaping (Error) -> Void) {
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        
        repository.getAllBusiness(success: { (response) in
            success(response)
        }) { (error) in
            onError(error)
        }
    }
}
