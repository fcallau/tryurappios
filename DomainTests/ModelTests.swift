//
//  ModelTests.swift
//  DomainTests
//
//  Created by Fernando Jarilla on 9/5/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import XCTest
@testable import Domain

class ModelTests: XCTestCase {
    
    var product1: Product!
    var products = Products()
    var post1: Post!
    var posts = Posts()
    
    override func setUp() {
        super.setUp()
        //Creamos un ProductWP
        product1 = Product(title: "Producto 1")
        
        //Creamos un conjunto de productosWP
        let product2 = Product(title: "Producto 2")
        let product3 = Product(title: "Producto 3")
        products.add(item: product1)
        products.add(item: product2)
        products.add(item: product3)
     
        //Creamos un PostWP
        post1 = Post(title: "Post 1")
        
        //Creamos un conjunto de postsWP
        let post2 = Post(title: "Post 2")
        let post3 = Post(title: "Post 3")
        posts.add(item: post1)
        posts.add(item: post2)
        posts.add(item: post3)

    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //TEST PRODUCT
    func testGiven_a_product_When_we_change_the_price_Then_the_price_changes() {
        product1.salePrice = 22.0
        XCTAssertTrue(product1.salePrice == 22.0)
    }
    
    //TEST PRODUCTS
    func testGiven_three_products_When_we_test_the_quantity_Then_the_quantity_is_three() {
        let total = products.count()
        XCTAssertTrue(total == 3)
    }
    
    func testGiven_three_products_When_we_get_element_2_Then_the_product_title_is_Producto_2() {
        let producto = products.get(index: 1)
        XCTAssertTrue(producto.title == "Producto 2")
    }
    
    //TEST POST
    func testGiven_a_post_When_we_change_the_writer_Then_the_writer_changes() {
        post1.writer = "Fernando"
        XCTAssertTrue(post1.writer == "Fernando")
    }
    
    //TEST POSTS
    func testGiven_three_posts_When_we_test_the_quantity_Then_the_quantity_is_three() {
        let total = posts.count()
        XCTAssertTrue(total == 3)
    }
    
    func testGiven_three_posts_When_we_get_element_2_Then_the_post_title_is_Post_2() {
        let post = posts.get(index: 1)
        XCTAssertTrue(post.title == "Post 2")
    }
}
