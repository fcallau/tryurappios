//
//  BusinessTableViewCell.swift
//  TryUrApp
//
//  Created by Francesc Callau Brull on 12/9/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit

class BusinessTableViewCell: UITableViewCell {

    @IBOutlet weak var url: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
