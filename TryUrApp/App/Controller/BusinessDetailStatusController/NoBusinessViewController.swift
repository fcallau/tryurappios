
import UIKit

class NoBusinessViewController: UIViewController {

    @IBOutlet weak var noBusinessLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        noBusinessLabel.text = "No business"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
