//
//  BusinessTableViewController.swift
//  TryUrApp
//
//  Created by Francesc Callau Brull on 10/9/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit

class BusinessTableViewController: UITableViewController {
    
    
    @IBOutlet var addBusinessBtn: UIBarButtonItem!
    
    var responseResult: [BusinessTUA] = []
    var businessSelected: BusinessTUA?
    var aUser: UserUI?
    var deleteBusinessInteractor: DeleteBusinessInteractor?
    var showNoDataMessage: Bool = false
    var messageCanBeDisplayedInBackgroundTableView: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("En BusinessTableViewController")
        
        self.navigationItem.rightBarButtonItem = nil
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        deleteBusinessInteractor = DeleteBusinessInteractorImpl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("in viewWillAppear")
        
        showNoDataMessage = false
        messageCanBeDisplayedInBackgroundTableView = false
        
        aUser = UserUI(email: "fcallau@yahoo.com", name: "?", password: "12345678")
        let getBusinessesByUserEmailInteractor = GetBusinessesByUserEmailInteractorImpl()
        
        // TOKEN & USEREMAIL
        // userEmail --> USEREMAIL
        // token --> TOKEN
        
        //spinner
        let sv = UIViewController.displaySpinner(onView: self.view)
        
        getBusinessesByUserEmailInteractor.execute(user: aUser!, success: { (response) in
            UIViewController.removeSpinner(spinner: sv)
            
            self.messageCanBeDisplayedInBackgroundTableView = true
            
            print("response.status: \(response.status)")
            
            if response.status == 200 {
                print("response.result!: \((response.result! as [BusinessTUA]).count)")
                self.responseResult = response.result!
            } else if response.status == 404 {
                self.responseResult = []
            }
            
            self.tableView.reloadData()
            self.navigationItem.rightBarButtonItem = self.addBusinessBtn
        }, onError: { (error) in
            UIViewController.removeSpinner(spinner: sv)
            self.showAlert(title: "Error getting businesses", message: error.localizedDescription, element: nil)
            print("error.localizedDescription: \(error.localizedDescription)")
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        print("numberOfSections")
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("numberOfRowsInSection")
        // #warning Incomplete implementation, return the number of rows
        print("responseResult.count: \(responseResult.count)")
        if self.messageCanBeDisplayedInBackgroundTableView {
            if responseResult.count == 0 {
                let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                
                noDataLabel.text          = "No data available"
                noDataLabel.textColor     = UIColor.darkGray
                noDataLabel.textAlignment = .center
                tableView.backgroundView  = noDataLabel
                tableView.separatorStyle  = .none
            } else {
                tableView.backgroundView = nil
            }
        }
        
        return responseResult.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessCell", for: indexPath) as! BusinessTableViewCell
        
        // Configure the cell...
        cell.url.text = responseResult[indexPath.row].url!
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        businessSelected = responseResult[indexPath.row]
        self.performSegue(withIdentifier: "BusinessBuildSegue", sender: nil)
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //spinner
            let sv = UIViewController.displaySpinner(onView: self.view)
            
            let aBusiness: BusinessUI = mapBusiness(from: responseResult[indexPath.row])
            deleteBusinessInteractor?.execute(business: aBusiness, user: aUser!, success: { (response) in
                UIViewController.removeSpinner(spinner: sv)
                // Delete the row from the data source
                self.responseResult.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            }, onError: { (error) in
                UIViewController.removeSpinner(spinner: sv)
                self.showAlert(title: "Error deleting businesses", message: error.localizedDescription, element: nil)
            })
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if (segue.destination is BusinessBuildViewController) {
            let vc = segue.destination as? BusinessBuildViewController
            vc?.businessInfo = businessSelected
        }
    }
    
    
    // MARK: - Several functions
    func showAlert(title: String, message: String, element: AnyObject?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let accept = UIAlertAction(title: "Accept", style: .default) { (action) in
            if (element?.window) != nil {
                if (element?.canBecomeFirstResponder)! {
                    _ = element?.becomeFirstResponder()
                }
            }
        }
        
        alert.addAction(accept)
        present(alert, animated: true, completion: nil)
    }
}
