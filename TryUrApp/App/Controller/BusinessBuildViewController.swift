//
//  BusinessBuildViewController.swift
//  TryUrApp
//
//  Created by Francesc Callau Brull on 13/9/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit

class BusinessBuildViewController: UIViewController {
    
    @IBOutlet weak var buildBtn: UIButton!
    
    var businessInfo: BusinessTUA?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let businessInfo = businessInfo {
            buildBtn.setTitle("Build app with url " + businessInfo.url!, for: .normal)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
