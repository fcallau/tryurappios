import UIKit

class AddBusinessViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    // BUSINESS prueba
    // let url: String = "https://www.url.business9.com"
    // var firstBusiness = BusinessUI(url: "https://www.url.business9.com", userEmail: "y1z1@gmail.com", idBusinessType: 1, idCmsType: 1)
    
    // USER
    // var user  = UserDefaults.standard.object(forKey: "USER") as? UserUI
    
    // UserDefaults.standard.set(response.resultDescription, forKey: "token")
    // UserDefaults.standard.set(user.email, forKey: "USER")
    
    // CMStypes
    // 0, WordPress
    // 1, Magento
    // 2, PrestaShop
    // 3, Joomla!
    
    // Businesstypes
    // 0, Ecommerce
    // 1, Blog
    // 2, Ecommerce+Blog
    // 3, Administration
    
    let pickerData = [String](arrayLiteral: "Wordpress", "Magento")
    let cmsTypes = [CmsType]()
    var business: Business?
    var activeField: UITextField?
    var scrollView: UIView?
    var yPositionScrollView: CGFloat = 0
    var yDisplacementMainView: CGFloat = 0
    var ySpaceBetweenFieldAndKeyboard: CGFloat = 0
    
    @IBOutlet weak var businessUrl: UITextField!
    @IBOutlet weak var cms: UITextField!
    @IBOutlet weak var ecommerceSwitch: UISwitch!
    @IBOutlet weak var blogSwitch: UISwitch!
    
    @IBOutlet weak var contentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Init
        cms.text = "Wordpress"
        
        businessUrl.delegate = self
        cms.delegate = self
        
        //delegate
        let thePicker = UIPickerView()
        
        thePicker.delegate = self
        cms.inputView = thePicker
        
        scrollView = self.view.subviews[0]
        yPositionScrollView = (scrollView?.frame.origin.y)!
        
        // self.view.bounds.origin.y = 200
        
        NotificationCenter.default.addObserver(self, selector: #selector(AddBusinessViewController.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AddBusinessViewController.keyboardWillHidde(notification:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // MARK: - UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        cms.text = pickerData[row]
        self.view.endEditing(true)
    }
    
    // MARK: - Several functions
    func showAlert(title: String, message: String, element: AnyObject?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let accept = UIAlertAction(title: "Accept", style: .default) { (action) in
            if (element?.window) != nil {
                if (element?.canBecomeFirstResponder)! {
                    _ = element?.becomeFirstResponder()
                }
            }
        }
        
        alert.addAction(accept)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - IBActions
    @IBAction func doneButton(_ sender: UIBarButtonItem) {
        //        let businessName = businessNameField?.text ?? ""
        //        business = Business(businessName: businessName)
        //        //TODO: Enganchar resto de los campos
        //
        //        //!! Guardamos negocio operacion asincrona
        //spinner
        let sv = UIViewController.displaySpinner(onView: self.view)
        //
        //        //Time out
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
        //            UIViewController.removeSpinner(spinner: sv)
        //            self.performSegue(withIdentifier: "saveBusiness", sender: self)
        //        }
        
        var allValidationsCorrectly = true
        
        var businessUrlChosen = ""
        var businessTypeChosen: Int64 = 0
        var cmsTypeChosen: Int64 = 0
        
        if businessUrl.text!.isEmpty {
            allValidationsCorrectly = false
            UIViewController.removeSpinner(spinner: sv)
            showAlert(title: "URL", message: "Business URL has to be informed", element: businessUrl)
        } else {
            businessUrlChosen = businessUrl.text!
        }
        
        if (allValidationsCorrectly) {
            if (cms.text!.isEmpty) {
                allValidationsCorrectly = false
                UIViewController.removeSpinner(spinner: sv)
                showAlert(title: "CMS type", message: "CMS type has to be informed", element: cms)
            } else {
                cmsTypeChosen = Int64(pickerData.index(of: cms.text!)!)
            }
        }
        
        if (allValidationsCorrectly) {
            if (!ecommerceSwitch.isOn && !blogSwitch.isOn) {
                allValidationsCorrectly = false
                UIViewController.removeSpinner(spinner: sv)
                showAlert(title: "Type section", message: "Ecommerce, blog or both have to be informed", element: nil)
            } else {
                if (ecommerceSwitch.isOn) {
                    // ecommerceSwitch.isOn && blogSwitch.isOn
                    if (blogSwitch.isOn) {
                        businessTypeChosen = 2
                    } else {
                        // ecommerceSwitch.isOn && !blogSwitch.isOn
                        businessTypeChosen = 1
                    }
                } else {
                    // !ecommerceSwitch.isOn && blogSwitch.isOn
                    businessTypeChosen = 0
                }
            }
        }
        
        if (allValidationsCorrectly) {
            // let aBusiness = BusinessUI(url: "https://www.myurl.com", userEmail: "y1z1@gmail.com", idBusinessType: businessTypeChosen, idCmsType: cmsTypeChosen)
            let aBusiness = BusinessUI(url: businessUrlChosen, userEmail: "fcallau@yahoo.com", idBusinessType: businessTypeChosen, idCmsType: cmsTypeChosen)
            let aUser: UserUI = UserUI(email: "fcallau@yahoo.com", name: "?", password: "12345678")
            
            let createBusinessInteractor = CreateBusinessInteractorImpl()
            
            // TOKEN & USEREMAIL
            
            createBusinessInteractor.execute(business: aBusiness,
                                             user: aUser,
                                             success: { (response) in
                                                UIViewController.removeSpinner(spinner: sv)
                                                if (response.status >= 400) {
                                                    self.showAlert(title: "Error creating business", message: response.resultDescription, element: nil)
                                                    print("response.resultDescription: \(response.resultDescription)")
                                                } else {
                                                    // UserDefaults.standard.set(aBusiness, forKey: "BUSINESS")
                                                    print("UserDefaults.standard.object(forKey: \"BUSINESS\"): \(UserDefaults.standard.object(forKey: "BUSINESS") ?? "HOLA CARACOLA")")
                                                    let myBusinessTUA: BusinessTUA = mapBusiness(from: aBusiness)
                                                    let data = NSKeyedArchiver.archivedData(withRootObject: myBusinessTUA)
                                                    UserDefaults.standard.set(data, forKey: "BUSINESS")
                                                    self.showAlert(title: "Business created", message: "Business was created", element: nil)
                                                    print("response.resultDescription: \(response.resultDescription)")
                                                } },
                                             onError: { (error) in
                                                UIViewController.removeSpinner(spinner: sv)
                                                self.showAlert(title: "Error creating business", message: error.localizedDescription, element: nil)
                                                print("error.localizedDescription: \(error.localizedDescription)") })
        }
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        //        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
        //            if let activeField = activeField {
        //                // print(self.view.frame.origin.y)
        //                //                print("(scrollView?.frame.origin.y)!: \((scrollView?.frame.origin.y)!)")
        //                //                print("scrollView?.frame.size.height: \(scrollView?.frame.size.height)")
        //                print("(scrollView?.bounds.origin.y (01))!: \((scrollView?.bounds.origin.y)!)")
        //                //                print("scrollView?.bounds.size.height: \(scrollView?.bounds.size.height)")
        //                print("yPositionScrollView: \(yPositionScrollView)")
        //                print("activeField.frame.origin.y: \(activeField.frame.origin.y)")
        //                print("activeField.frame.height: \(activeField.frame.height)")
        //                print("ySpaceBetweenFieldAndKeyboard: \(ySpaceBetweenFieldAndKeyboard)")
        //                print("keyboardSize.origin.y: \(keyboardSize.origin.y)")
        //                if (yPositionScrollView + activeField.frame.origin.y + activeField.frame.height + ySpaceBetweenFieldAndKeyboard > keyboardSize.origin.y) {
        //                    yDisplacementMainView = (yPositionScrollView + activeField.frame.origin.y + activeField.frame.height + ySpaceBetweenFieldAndKeyboard - keyboardSize.origin.y)
        //                    print(">>> yDisplacementMainView: \(yDisplacementMainView)")
        //                    // self.view.frame.origin.y = self.view.frame.origin.y - yDisplacementMainView
        //                    // self.view.bounds.origin.y = self.view.bounds.origin.y + yDisplacementMainView
        //                    scrollView?.bounds.origin.y = 239
        //                    print("self.view.frame.origin.y: \(self.view.frame.origin.y)")
        //                    print("self.view.bounds.origin.y: \(self.view.bounds.origin.y)")
        //                    print("(scrollView?.bounds.origin.y (02))!: \((scrollView?.bounds.origin.y)!)")
        //                    print("contentView.frame.origin.y: \(contentView.frame.origin.y)")
        //                    print("contentView.bounds.origin.y: \(contentView.bounds.origin.y)")
        //                }
        //            }
        //        }
    }
    
    @objc func keyboardWillHidde(notification: Notification) {
        //        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
        //            yDisplacementMainView = 0
        //        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeField = nil
    }
}

class CmsType: NSObject {
    var CmsTypeId: uint!
    var CmsTypeLiteral: String!
    
    init?(from jsonObject: AnyObject) {
        guard let cId = jsonObject.object(forKey: "idCmsType") as? uint,
            let cLiteral = jsonObject.object(forKey: "literal") as? String else {
                print("Error creating CMStype object")
                return nil
        }
        
        CmsTypeId = cId
        CmsTypeLiteral = cLiteral
        
        super.init()
    }
}
