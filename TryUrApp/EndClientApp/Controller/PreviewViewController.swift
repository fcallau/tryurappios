//
//  PreviewViewController.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 9/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit

class PreviewViewController: UIViewController {

    //USERS
    var firstUser = UserUI(email: "y1z1@gmail.com", name: "Fernando", password: "Jarilla1")
    
    @IBAction func getAllUsers(_ sender: Any) {
        getUsers()
    }
    @IBAction func getUser(_ sender: Any) {
        getUser()
    }
    @IBAction func addUser(_ sender: Any) {
        addUser()
    }
    @IBAction func deleteUser(_ sender: Any) {
        deleteUser()
    }
    @IBAction func updateUser(_ sender: Any) {
        updateUser()
    }
    @IBAction func authenticateUser(_ sender: Any) {
        authUser()
    }
    
    //BUSINESS
    var firstBusiness = BusinessUI(url: "https://www.url.business9.com", userEmail: "y1z1@gmail.com", idBusinessType: 1, idCmsType: 1)
    
    
    @IBAction func buildApp(_ sender: Any) {
        var name = ""
        if (businessName.text != nil) { name = businessName.text! } else { name = "" }
        buildApplication(applicationName: name)
    }
    
    @IBOutlet weak var businessName: UITextField!
    
    @IBAction func createBusiness(_ sender: Any) {
        createBusiness()
    }
    @IBAction func SaveBusinessSettings(_ sender: Any) {
        saveBusinessSettings()
    }
    @IBAction func uploadLogo(_ sender: Any) {
        uploadLogo()
    }
    @IBAction func uploadWelcome(_ sender: Any) {
        uploadWelcome()
    }
    @IBAction func getBusiness(_ sender: Any) {
        getBusiness()
    }
    @IBAction func getAllBusiness(_ sender: Any) {
        getAllBusiness()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //GENERALES
    func mostrarAlerta(title: String, message: String) {
        let alertaGuia = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let aceptar = UIAlertAction(title: "Aceptar", style: .default) { (action) in
        }
        alertaGuia.addAction(aceptar)
        present(alertaGuia, animated: true, completion: nil)
    }
    
    //USERS
    func getUsers() {
        let getAllUsersInteractor = GetAllUsersInteractorImpl()
        getAllUsersInteractor.execute(success: { (response) in
            self.mostrarAlerta(title: "USERS", message: "Recogidos USERS: \(response.resultDescription)")
            print(response.result)
        }) { (error) in
            self.mostrarAlerta(title: "ERROR", message: error.localizedDescription)
        }
    }
    
    func getUser() {
        let getUserInteractor = GetUserInteractorImpl()
        getUserInteractor.execute(user: firstUser, success: { (response) in
            self.mostrarAlerta(title: "USER", message: response.resultDescription)
            print(response.result)
        }) { (error) in
            print("Error: ", error.localizedDescription)
            self.mostrarAlerta(title: "ERROR", message: error.localizedDescription)
        }
    }
    
    func authUser() {
        let authUserInteractor = AuthenticateUserInteractorImpl()
        authUserInteractor.execute(user: firstUser, success: { (response) in
            self.mostrarAlerta(title: "USER", message: response.resultDescription)
            print(response.result)
        }) { (error) in
            print("Error: ", error.localizedDescription)
            self.mostrarAlerta(title: "ERROR", message: error.localizedDescription)
        }
    }
    
    func addUser() {
        let addUserInteractor = AddUserInteractorImpl()
        addUserInteractor.execute(user: firstUser, success: { (response) in
            self.mostrarAlerta(title: "USER", message: response.resultDescription)
            print(response.result)
        }) { (error) in
            print("Error: ", error.localizedDescription)
            self.mostrarAlerta(title: "ERROR", message: error.localizedDescription)
        }
    }
    
    func updateUser() {
        let updateUserInteractor = UpdateUserInteractorImpl()
        firstUser.name = "Fernando updated"
        updateUserInteractor.execute(user: firstUser, success: { (response) in
            self.mostrarAlerta(title: "USER", message: response.resultDescription)
            print(response.result)
        }) { (error) in
            print("Error: ", error.localizedDescription)
            self.mostrarAlerta(title: "ERROR", message: error.localizedDescription)
        }
    }
    
    func deleteUser() {
        let deleteUserInteractor = DeleteUserInteractorImpl()
        deleteUserInteractor.execute(user: firstUser, success: { (response) in
            self.mostrarAlerta(title: "USER", message: response.resultDescription)
            print(response.result)
        }) { (error) in
            print("Error: ", error.localizedDescription)
            self.mostrarAlerta(title: "ERROR", message: error.localizedDescription)
        }
    }
    
    //BUSINESS
    func createBusiness() {
        let createBusinessInteractor = CreateBusinessInteractorImpl()
        createBusinessInteractor.execute(business: firstBusiness, user: firstUser, success: { (response) in
            self.mostrarAlerta(title: "USER", message: response.resultDescription)
            print(response.result)
        }) { (error) in
            print("Error: ", error.localizedDescription)
            self.mostrarAlerta(title: "ERROR", message: error.localizedDescription)
        }
    }
    
    func saveBusinessSettings() {
        firstBusiness.idBusinessType = 1
        firstBusiness.idAppIcon = 1
        firstBusiness.idLoadingType = 1
        firstBusiness.firstColorIdentifier = 1
        firstBusiness.secondColorIdentifier = 1
        firstBusiness.thirdColorIdentifier = 1
        firstBusiness.idFont = 1
        firstBusiness.buildApp = "false"
        firstBusiness.businessName = "TryUrApp"
        let saveBusinessSettingsInteractor = SaveBusinessSettingsInteractorImpl()
        saveBusinessSettingsInteractor.execute(business: firstBusiness, user: firstUser, success: { (response) in
            self.mostrarAlerta(title: "USER", message: response.resultDescription)
            print(response.result)
        }) { (error) in
            print("Error: ", error.localizedDescription)
            self.mostrarAlerta(title: "ERROR", message: error.localizedDescription)
        }
    }
    
    func getAllBusiness() {
        let getAllBusinessesInteractor = GetAllBusinessesInteractorImpl()
        getAllBusinessesInteractor.execute(success: { (response) in
            self.mostrarAlerta(title: "USERS", message: "Recogidos BUSINESSES: \(response.resultDescription)")
            print(response.result)
        }) { (error) in
            self.mostrarAlerta(title: "ERROR", message: error.localizedDescription)
        }
    }
    
    func getBusiness() {
        let getBusinessInteractor = GetBusinessInteractorImpl()
        getBusinessInteractor.execute(business: firstBusiness, user: firstUser, success: { (response) in
            self.mostrarAlerta(title: "USER", message: response.resultDescription)
            print(response.result)
        }) { (error) in
            print("Error: ", error.localizedDescription)
            self.mostrarAlerta(title: "ERROR", message: error.localizedDescription)
        }
    }
    
    func uploadLogo() {
        let uploadImageInteractor = UploadImageInteractorImpl()
        uploadImageInteractor.execute(business: firstBusiness, user: firstUser, imageType: "logo", image: "logoTUA", success: { (response) in
            self.mostrarAlerta(title: "LOGO", message: response.resultDescription)
            print(response.result)
        }) { (error) in
            print("Error: ", error.localizedDescription)
            self.mostrarAlerta(title: "ERROR", message: error.localizedDescription)
        }
    }
    
    func uploadWelcome() {
        let uploadImageInteractor = UploadImageInteractorImpl()
        uploadImageInteractor.execute(business: firstBusiness, user: firstUser, imageType: "welcomeImage", image: "welcomePage", success: { (response) in
            self.mostrarAlerta(title: "WELCOME PAGE", message: response.resultDescription)
            print(response.result)
        }) { (error) in
            print("Error: ", error.localizedDescription)
            self.mostrarAlerta(title: "ERROR", message: error.localizedDescription)
        }
    }
    
    func buildApplication(applicationName: String) {
        
        firstBusiness.businessName = applicationName
        firstBusiness.buildApp = "ios"
        firstBusiness.idBusinessType = 1
        firstBusiness.idAppIcon = 1
        firstBusiness.idLoadingType = 1
        firstBusiness.firstColorIdentifier = 1
        firstBusiness.secondColorIdentifier = 1
        firstBusiness.thirdColorIdentifier = 1
        firstBusiness.idFont = 1
        
        let saveBusinessSettingsInteractor = SaveBusinessSettingsInteractorImpl()
        saveBusinessSettingsInteractor.execute(business: firstBusiness, user: firstUser, success: { (response) in
            self.mostrarAlerta(title: "USER", message: response.resultDescription)
            print(response.result)
        }) { (error) in
            print("Error: ", error.localizedDescription)
            self.mostrarAlerta(title: "ERROR", message: error.localizedDescription)
        }
    }
}
