//
//  RepositoryTUATests.swift
//  TryUrAppTests
//
//  Created by Fernando Jarilla on 7/5/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import XCTest
@testable import TryUrApp

class RepositoryTUAImplTests: XCTestCase {
    
    var userTUA: UserTUA!
    var businessTUA: BusinessTUA!
    
    override func setUp() {
        super.setUp()
        
        //Creamos un userTUA
        userTUA = UserTUA(email: "usuarioTest@gmail.com")
        userTUA.name = "Fernando Test"
        userTUA.password = "Jarilla1"
        
        //Creamos un businessTUA
        businessTUA = BusinessTUA(url: "https://www.businessTest.com",
                                  userEmail: "businessTest@gmail.com",
                                  idBusinessType: 1,
                                  idCmsType: 1)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    func test_get_user_works_properly() {
        let ex = expectation(description: "Expecting a getUser response")
        
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        repository.getUser(user: userTUA, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_get_all_users_works_properly() {
        let ex = expectation(description: "Expecting a getAllUsers response")
        
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        
        repository.getAllUsers(success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_auth_user_works_properly() {
        let ex = expectation(description: "Expecting a authUser response")
        
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        repository.authUser(user: userTUA, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_add_user_works_properly() {
        let ex = expectation(description: "Expecting a addUser response")
        
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        repository.addUser(user: userTUA, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_update_user_works_properly() {
        let ex = expectation(description: "Expecting a updateUser response")
        
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        repository.updateUser(user: userTUA, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_delete_user_works_properly() {
        let ex = expectation(description: "Expecting a deleteUser response")
        
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        repository.deleteUser(user: userTUA, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_create_business_works_properly() {
        let ex = expectation(description: "Expecting a createBusiness response")
        
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        repository.createBusiness(business: businessTUA, user: userTUA, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_saveSettings_business_works_properly() {
        let ex = expectation(description: "Expecting a saveSettingsBusiness response")
        
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        repository.saveBusinessSettings(business: businessTUA, user: userTUA, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_get_business_works_properly() {
        let ex = expectation(description: "Expecting a getBusiness response")
        
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        repository.getBusiness(business: businessTUA, user: userTUA, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_get_all_businesses_works_properly() {
        let ex = expectation(description: "Expecting a getAllBusinesses response")
        
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        
        repository.getAllBusiness(success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_upload_image_works_properly() {
        let ex = expectation(description: "Expecting a uploadImage response")
        let imageType = "welcomeImage"
        let image = "welcomePage"
        
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        repository.uploadImage(business: businessTUA, user: userTUA, imageType: imageType, image: image, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_build_app_works_properly() {
        let ex = expectation(description: "Expecting a uploadImage response")
        
        let repository: RepositoryTUA = RepositoryTUAAlamofireImpl()
        repository.buildApplication(business: businessTUA, user: userTUA, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
   
}
