//
//  RepositoryTUAAlamofireImpl.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 19/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import SwiftyJSON

public class RepositoryTUAAlamofireImpl: RepositoryTUA {
    
    private let decoder = JSONDecoder()

    //USERS
    public func getAllUsers(success: @escaping ResponseUsersSuccessClosure, errorClosure: ErrorClosure?) {
        let endpoint = "users/all"
        let param: [String: String] = [:]
        let request = endpointRequest(adding: param, and: endpoint)
        
        Alamofire.request(request)
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    var res = ResponseUsersTUA()
                    do {
                        res = try JSONDecoder().decode(ResponseUsersTUA.self, from: response.data!)
                    } catch {
                        let error = AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                        errorClosure!(error)
                    }
                    success(res)
                case .failure(let error):
                    errorClosure!(error)
                }
        }
    }
    
    public func getUser(user: UserTUA, success: @escaping ResponseSuccessClosure, errorClosure: ErrorClosure?) {
        //Autentico el usuario para obtener el token
        authUser(user: user, success: { (response) in
            //tengo el usuario autenticado
            let endpoint = "users/"
            let param = ["email": user.email,
                         "token": response.resultDescription]
            let request = endpointRequest(adding: param, and: endpoint)
            
            Alamofire.request(request)
                .responseJSON { (response) in
                    switch response.result {
                    case .success:
                        var res = ResponseTUA()
                        do {
                            res = try JSONDecoder().decode(ResponseTUA.self, from: response.data!)
                        } catch {
                            let error = AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                            errorClosure!(error)
                        }
                        success(res)
                    case .failure(let error):
                        errorClosure!(error)
                    }
            }
        }) { (error) in
            //El usuario no está autenticado
            errorClosure!(error)
        }
    }
    
    public func authUser(user: UserTUA, success: @escaping ResponseSuccessClosure, errorClosure: ErrorClosure?) {
        let endpoint = "users/authenticate"
        let param = ["email": user.email,
                    "password": user.password]
        let request = endpointRequest(adding: param as! [String : String], and: endpoint)
        
        Alamofire.request(request)
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    var res = ResponseTUA()
                    do {
                        res = try JSONDecoder().decode(ResponseTUA.self, from: response.data!)
                    } catch {
                        let error = AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                        errorClosure!(error)
                    }
                    success(res)
                case .failure(let error):
                    errorClosure!(error)
                }
            }
    }
    
    public func addUser(user: UserTUA, success: @escaping ResponseSuccessClosure, errorClosure: ErrorClosure?) {
        let endpoint = "users/register"
        let body = ["email": user.email,
                    "name": user.name,
                    "password": user.password]
        let token = ["token": ""]
        let headers = ["Content-Type": "application/json"]
        let request = endpointRequest(adding: token, and: endpoint)
        
        Alamofire.request("\(request)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    var res = ResponseTUA()
                    do {
                        res = try JSONDecoder().decode(ResponseTUA.self, from: response.data!)
                    } catch {
                        let error = AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                        errorClosure!(error)
                    }
                    success(res)
                case .failure(let error):
                    errorClosure!(error)
                }
        }
    }
    
    public func updateUser(user: UserTUA, success: @escaping ResponseSuccessClosure, errorClosure: ErrorClosure?) {
        //Autentico el usuario para obtener el token
        authUser(user: user, success: { (response) in
            //tengo el usuario autenticado
            let endpoint = "users/update"
            let body = ["token": response.resultDescription,
                        "email": user.email,
                        "name": user.name]
            let token: [String: String] = [:]
            let headers = ["Content-Type": "application/json"]
            let request = endpointRequest(adding: token, and: endpoint)
            
            Alamofire.request("\(request)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
                .responseJSON { (response) in
                    switch response.result {
                    case .success:
                        var res = ResponseTUA()
                        do {
                            res = try JSONDecoder().decode(ResponseTUA.self, from: response.data!)
                        } catch {
                            let error = AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                            errorClosure!(error)
                        }
                        success(res)
                    case .failure(let error):
                        errorClosure!(error)
                    }
            }
        }) { (error) in
            //El usuario no está autenticado
            errorClosure!(error)
        }
    }
    
    public func deleteUser(user: UserTUA, success: @escaping ResponseSuccessClosure, errorClosure: ErrorClosure?) {
        //Autentico el usuario para obtener el token
        authUser(user: user, success: { (response) in
            //tengo el usuario autenticado
            let endpoint = "users/delete"
            let body = ["email": user.email,
                        "token": response.resultDescription] as [String : Any]
            let token: [String: String] = [:]
            let headers = ["Content-Type": "application/json"]
            let request = endpointRequest(adding: token, and: endpoint)
            
            Alamofire.request("\(request)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
                .responseJSON { (response) in
                    switch response.result {
                    case .success:
                        var res = ResponseTUA()
                        do {
                            res = try JSONDecoder().decode(ResponseTUA.self, from: response.data!)
                        } catch {
                            let error = AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                            errorClosure!(error)
                        }
                        success(res)
                    case .failure(let error):
                        errorClosure!(error)
                    }
            }
        }) { (error) in
            //El usuario no está autenticado
            errorClosure!(error)
        }
    }
    
    //BUSINESS
    public func createBusiness(business: BusinessTUA, user: UserTUA, success: @escaping ResponseBusinessSuccessClosure, errorClosure: ErrorClosure?) {
        //Autentico el usuario para obtener el token
        authUser(user: user, success: { (response) in
            //tengo el usuario autenticado
            let endpoint = "businesses/create"
            let body: [String: Any] = ["token": response.resultDescription,
                                       "userEmail": business.userEmail!,
                                       "url": business.url!,
                                       "idBusinessType": String(business.idBusinessType!),
                                       "idCmsType": String(business.idCmsType!)]
            let token: [String: String] = [:]
            let headers = ["Content-Type": "application/json"]
            let request = endpointRequest(adding: token, and: endpoint)
            print(request)
            print(body)
            
            Alamofire.request("\(request)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
                .responseJSON { (response) in
                    switch response.result {
                    case .success:
                        var res = ResponseBusinessTUA()
                        do {
                            res = try JSONDecoder().decode(ResponseBusinessTUA.self, from: response.data!)
                        } catch {
                            let error = AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                            errorClosure!(error)
                        }
                        print(response)
                        print(res)
                        success(res)
                    case .failure(let error):
                        errorClosure!(error)
                    }
            }
        }) { (error) in
            //El usuario no está autenticado
            errorClosure!(error)
        }
    }
    
    public func deleteBusiness(business: BusinessTUA, user: UserTUA, success: @escaping ResponseBusinessSuccessClosure, errorClosure: ErrorClosure?) {
        //Autentico el usuario para obtener el token
        authUser(user: user, success: { (response) in
            //tengo el usuario autenticado
            let endpoint = "businesses/delete"
            let body: [String: Any] = ["token": response.resultDescription,
                                       "url": business.url!]
            let token: [String: String] = [:]
            let headers = ["Content-Type": "application/json"]
            let request = endpointRequest(adding: token, and: endpoint)
            print(request)
            print(body)
            
            Alamofire.request("\(request)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
                .responseJSON { (response) in
                    switch response.result {
                    case .success:
                        var res = ResponseBusinessTUA()
                        do {
                            res = try JSONDecoder().decode(ResponseBusinessTUA.self, from: response.data!)
                        } catch {
                            let error = AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                            errorClosure!(error)
                        }
                        print(response)
                        print(res)
                        success(res)
                    case .failure(let error):
                        errorClosure!(error)
                    }
            }
        }) { (error) in
            //El usuario no está autenticado
            errorClosure!(error)
        }
    }
    
    public func saveBusinessSettings(business: BusinessTUA, user: UserTUA, success: @escaping ResponseBusinessSuccessClosure, errorClosure: ErrorClosure?) {
        //Autentico el usuario para obtener el token
        authUser(user: user, success: { (response) in
            //tengo el usuario autenticado
            let endpoint = "businesses/savesettings"
            let body: [String: Any] = ["token": response.resultDescription,
                                       "url": business.url!,
                                       "idLoadingType": business.idLoadingType!,
                                       "idAppIcon": business.idAppIcon!,
                                       "firstColorIdentifier": business.firstColorIdentifier!,
                                       "secondColorIdentifier": business.secondColorIdentifier!,
                                       "thirdColorIdentifier": business.thirdColorIdentifier!,
                                       "idFont": business.idFont!,
                                       "buildApp": "false",
                                       "businessName": business.businessName!]
            let token: [String: String] = [:]
            let headers = ["Content-Type": "application/json"]
            let request = endpointRequest(adding: token, and: endpoint)
            print(request)
            
            Alamofire.request("\(request)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
                .responseJSON { (response) in
                    switch response.result {
                    case .success:
                        var res = ResponseBusinessTUA()
                        do {
                            res = try JSONDecoder().decode(ResponseBusinessTUA.self, from: response.data!)
                        } catch {
                            let error = AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                            errorClosure!(error)
                        }
                        print(response)
                        print(res)
                        success(res)
                    case .failure(let error):
                        errorClosure!(error)
                    }
            }
        }) { (error) in
            //El usuario no está autenticado
            errorClosure!(error)
        }
    }
    
    public func getBusiness(business: BusinessTUA, user: UserTUA, success: @escaping ResponseBusinessSuccessClosure, errorClosure: ErrorClosure?) {
        //Autentico el usuario para obtener el token
        authUser(user: user, success: { (response) in
            //tengo el usuario autenticado
            let endpoint = "businesses/"
            let param = ["url": business.url,
                         "token": response.resultDescription]
            let request = endpointRequest(adding: param as! [String : String], and: endpoint)
            
            Alamofire.request(request)
                .responseJSON { (response) in
                    switch response.result {
                    case .success:
                        var res = ResponseBusinessTUA()
                        do {
                            res = try JSONDecoder().decode(ResponseBusinessTUA.self, from: response.data!)
                        } catch {
                            let error = AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                            errorClosure!(error)
                        }
                        success(res)
                    case .failure(let error):
                        errorClosure!(error)
                    }
            }
        }) { (error) in
            //El usuario no está autenticado
            errorClosure!(error)
        }
    }
    
    public func getBusinessesByUserEmail(user: UserTUA, success: @escaping ResponseBusinessesSuccessClosure, errorClosure: ErrorClosure?) {
        authUser(user: user, success: { (response) in
            let endpoint = "businesses/byUserEmail"
            let param = ["userEmail": user.email, "token": response.resultDescription]
            let request = endpointRequest(adding: param, and: endpoint)

            Alamofire.request(request).responseJSON { (response) in
                switch response.result {
                case .success:
                    var res = ResponseBusinessesTUA()
                    do {
                        res = try JSONDecoder().decode(ResponseBusinessesTUA.self, from: response.data!)
                    } catch {
                        let error = AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                        errorClosure!(error)
                    }
                    success(res)
                case .failure(let error):
                    errorClosure!(error)
                }
            }
        }) { (error) in
            errorClosure!(error)
        }
    }
    
    public func getAllBusiness(success: @escaping ResponseBusinessesSuccessClosure, errorClosure: ErrorClosure?) {
        let endpoint = "businesses/all"
        let param: [String: String] = [:]
        let request = endpointRequest(adding: param, and: endpoint)
        
        Alamofire.request(request)
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    var res = ResponseBusinessesTUA()
                    do {
                        res = try JSONDecoder().decode(ResponseBusinessesTUA.self, from: response.data!)
                    } catch {
                        let error = AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                        errorClosure!(error)
                    }
                    success(res)
                case .failure(let error):
                    errorClosure!(error)
                }
        }
    }
    
    public func uploadImage(business: BusinessTUA, user: UserTUA, imageType: String, image: String, success: @escaping ResponseBusinessSuccessClosure, errorClosure: ErrorClosure?) {
        //Autentico el usuario para obtener el token
        authUser(user: user, success: { (response) in
            //tengo el usuario autenticado
            let endpoint = "businesses/uploadImage"
            let param = ["url": business.url!,
                         "token": response.resultDescription,
                         "imageType": imageType]
            let imageUI = UIImage(named: image)
            let request = endpointRequest(adding: param, and: endpoint)
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                if let imageData = UIImageJPEGRepresentation(imageUI!, 1) {
                    multipartFormData.append(imageData, withName: "image", fileName: (image+".png"), mimeType: (image+"/png"))
                }
                for (key, value) in param {
                    multipartFormData.append((value.data(using: .utf8))!, withName: key) }
            }, to: "\(request)",
                    encodingCompletion: { encodingResult in
                        switch encodingResult {
                        case .success(let upload, _, _):
                            upload.uploadProgress(closure: { (progress) in
                                print("Upload Progress: \(progress.fractionCompleted)") //Implement a progress Controller
                            })
                            upload.responseJSON { response in
                                if let jsonResponse = response.result.value as? [String: Any] {
                                    print(jsonResponse)
                                    var res = ResponseBusinessTUA()
                                    do {
                                        res = try JSONDecoder().decode(ResponseBusinessTUA.self, from: response.data!)
                                    } catch {
                                        let error = AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                                        errorClosure!(error)
                                    }
                                    success(res)
                                }
                            }
                        case .failure(let encodingError):
                            print("error:\(encodingError)")
                            errorClosure!(encodingError)
                        }
            })
        }) { (error) in
            //El usuario no está autenticado
            errorClosure!(error)
        }
    }
    
    public func buildApplication(business: BusinessTUA, user: UserTUA, success: @escaping ResponseBusinessSuccessClosure, errorClosure: ErrorClosure?) {
        //Autentico el usuario para obtener el token
        authUser(user: user, success: { (response) in
            //tengo el usuario autenticado
            let endpoint = "businesses/savesettings"
            let body: [String: Any] = ["token": response.resultDescription,
                                       "url": business.url!,
                                       "idLoadingType": business.idLoadingType!,
                                       "idAppIcon": business.idAppIcon!,
                                       "firstColorIdentifier": business.firstColorIdentifier!,
                                       "secondColorIdentifier": business.secondColorIdentifier!,
                                       "thirdColorIdentifier": business.thirdColorIdentifier!,
                                       "idFont": business.idFont!,
                                       "buildApp": business.businessName!,
                                       "businessName": business.businessName!]
            let token: [String: String] = [:]
            let headers = ["Content-Type": "application/json"]
            let request = endpointRequest(adding: token, and: endpoint)
            print(request)
            
            Alamofire.request("\(request)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
                .responseJSON { (response) in
                    switch response.result {
                    case .success:
                        var res = ResponseBusinessTUA()
                        do {
                            res = try JSONDecoder().decode(ResponseBusinessTUA.self, from: response.data!)
                        } catch {
                            let error = AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                            errorClosure!(error)
                        }
                        print(response)
                        print(res)
                        success(res)
                    case .failure(let error):
                        errorClosure!(error)
                    }
            }
        }) { (error) in
            //El usuario no está autenticado
            errorClosure!(error)
        }
    }
}
