//
//  UsersTUAProtocol.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 9/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

protocol UsersTUAProtocol {
    func count() -> Int
    mutating func add(user: UserTUA)
    func get(index: Int) -> UserTUA
}
