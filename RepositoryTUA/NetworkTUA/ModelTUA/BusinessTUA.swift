//
//  BusinessTUA.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 20/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

// public struct BusinessTUA: Decodable {
// public class BusinessTUA: Decodable {
public class BusinessTUA: NSObject, Decodable, NSCoding {
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.url, forKey: "url")
        aCoder.encode(self.userEmail, forKey: "userEmail")
        aCoder.encode(self.idBusinessType, forKey: "idBusinessType")
        aCoder.encode(self.idCmsType, forKey: "idCmsType")
    }
    
    public required convenience init?(coder aDecoder: NSCoder) {
        guard let url = aDecoder.decodeObject(forKey: "url") as? String,
        let userEmail = aDecoder.decodeObject(forKey: "userEmail") as? String,
        let idBusinessType = aDecoder.decodeObject(forKey: "idBusinessType") as? Int64,
        let idCmsType = aDecoder.decodeObject(forKey: "idCmsType") as? Int64
        else { return nil }
        
        self.init(url: url, userEmail: userEmail, idBusinessType: idBusinessType, idCmsType: idCmsType)
    }
    
    //var token: String?
    var userEmail: String?
    var url: String? = ""
    var idBusinessType: Int64? = 0
    var idCmsType: Int64? = 0
    var idLoadingType: Int64? = 0
    var idAppIcon: Int64? = 0
    var firstColorIdentifier: Int64? = 0
    var secondColorIdentifier: Int64? = 0
    var thirdColorIdentifier: Int64? = 0
    var idFont: Int64? = 0
    var isValidInfo: Bool? = true
    var logoDataName: String? = ""
    var welcomeImageDataName: String? = ""
    var buildApp: String? = ""
    var businessName: String? = ""
    
    private enum CodingKeys: String, CodingKey {
        //case token
        case userEmail
        case url
        case idBusinessType
        case idCmsType
        case idLoadingType
        case idAppIcon
        case secondColorIdentifier
        case thirdColorIdentifier
        case idFont
        case isValidInfo
        case logoDataName
        case welcomeImageDataName
        case buildApp
        case businessName
    }
    
    public init(url: String, userEmail: String, idBusinessType: Int64, idCmsType: Int64) {
        self.url = url
        self.userEmail = userEmail
        self.idBusinessType = idBusinessType
        self.idCmsType = idCmsType
    }
}
