//
//  ResponseBusinessesTUA.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 25/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public struct ResponseBusinessesTUA: Decodable {
    public var status: Int = 0
    public var resultDescription: String = ""
    public var result: [BusinessTUA]? = nil
}
