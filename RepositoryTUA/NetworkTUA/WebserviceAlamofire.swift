//
//  WebserviceAlamofire.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 20/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

func endpointRequest(adding parameters: [String: String], and path: String) -> URLRequest {
        
    //let baseURL = URL(string: "https://api.tryurapp.com/api/")!
    let baseURL = URL(string: "http://localhost:5407/api/")!
    //let baseURL = URL(string: "http://localhost:9090/")!
        
    let url = baseURL.appendingPathComponent(path)
        
    var newParameters: [String: String] = [:]
    parameters.forEach { newParameters.updateValue($1, forKey: $0) }
        
    var components = URLComponents(url: url, resolvingAgainstBaseURL: false)!
    components.queryItems = newParameters.map(URLQueryItem.init)
        
    let request = URLRequest(url: components.url!)
        
    return request
}
