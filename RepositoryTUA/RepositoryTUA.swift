//
//  RepositoryTUA.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 9/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import RealmSwift

public protocol RepositoryTUA {
    //USERS
    func getAllUsers(success: @escaping ResponseUsersSuccessClosure, errorClosure: ErrorClosure?)
    func getUser(user: UserTUA, success: @escaping ResponseSuccessClosure, errorClosure: ErrorClosure?)
    func authUser(user: UserTUA, success: @escaping ResponseSuccessClosure, errorClosure: ErrorClosure?)
    func addUser(user: UserTUA, success: @escaping ResponseSuccessClosure, errorClosure: ErrorClosure?)
    func updateUser(user: UserTUA, success: @escaping ResponseSuccessClosure, errorClosure: ErrorClosure?)
    func deleteUser(user: UserTUA, success: @escaping ResponseSuccessClosure, errorClosure: ErrorClosure?)
    //BUSINESS
    func createBusiness(business: BusinessTUA, user: UserTUA, success: @escaping ResponseBusinessSuccessClosure, errorClosure: ErrorClosure?)
    func deleteBusiness(business: BusinessTUA, user: UserTUA, success: @escaping ResponseBusinessSuccessClosure, errorClosure: ErrorClosure?)
    func saveBusinessSettings(business: BusinessTUA, user: UserTUA, success: @escaping ResponseBusinessSuccessClosure, errorClosure: ErrorClosure?)
    func getBusiness(business: BusinessTUA, user: UserTUA, success: @escaping ResponseBusinessSuccessClosure, errorClosure: ErrorClosure?)
    func getBusinessesByUserEmail(user: UserTUA, success: @escaping ResponseBusinessesSuccessClosure, errorClosure: ErrorClosure?)
    func getAllBusiness(success: @escaping ResponseBusinessesSuccessClosure, errorClosure: ErrorClosure?)
    func uploadImage(business: BusinessTUA, user: UserTUA, imageType: String, image: String, success: @escaping ResponseBusinessSuccessClosure, errorClosure: ErrorClosure?)
    func buildApplication(business: BusinessTUA, user: UserTUA, success: @escaping ResponseBusinessSuccessClosure, errorClosure: ErrorClosure?)
}
