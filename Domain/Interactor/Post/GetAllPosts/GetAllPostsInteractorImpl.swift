//
//  GetAllPostsInteractorImpl.swift
//  Domain
//
//  Created by Fernando Jarilla on 8/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import Repository

public class GetAllPostsInteractorImpl: GetAllItems {
    
    public typealias ItemsType = Posts
    
    public init() {}
    
    public func execute(success: @escaping (Posts) -> Void, onError: @escaping (Error) -> Void) {
        let repository: Repository = RepositoryImpl()
        
        repository.getAllPosts(success: { (items) in
            var posts = Posts()
            
            items.forEach({ (item) in
                let post = mapPost(from: item)
                posts.add(item: post)
            })
            success(posts)
            
        }) { (error) in
            onError(error)
        }
    }
}
