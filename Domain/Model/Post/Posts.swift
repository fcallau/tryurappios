//
//  Posts.swift
//  Domain
//
//  Created by Fernando Jarilla on 8/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public struct Posts: Agregate {
    public typealias AgregateType = Post
    
    private var _posts: [Post]?
    
    public init() {
        self._posts = []
    }
    
    public func count() -> Int {
        return (_posts?.count)!
    }
    
    public func getAll() -> [Post] {
        return _posts!
    }
    
    public func get(index: Int) -> Post {
        return _posts![index]
    }
    
    public mutating func add(item: Post) {
        _posts?.append(item)
    }
    
    
    
    
}
