//
//  PostsWP.swift
//  Repository
//
//  Created by Fernando Jarilla on 8/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import RealmSwift

public struct PostsWP: PostsWPProtocol, Decodable {
    
    private var postsWP: [PostWP]?
    
    public init() {
        self.postsWP = []
    }
    
    public func count() -> Int {
        return (postsWP?.count)!
    }
    
    public mutating func add(post: PostWP) {
        postsWP?.append(post)
    }
    
    public func get(index: Int) -> PostWP {
        return (postsWP?[index])!
    }
}
