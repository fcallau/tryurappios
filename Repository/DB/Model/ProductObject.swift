//
//  Product.swift
//  Repository
//
//  Created by Henry Bravo on 3/23/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

public final class ProductObject: Object {
    @objc dynamic public var identifier: Int64 = 0
    @objc dynamic public var title: String = ""
    @objc dynamic public var image: String = ""
    @objc dynamic public var salePrice: Double = 0.0
    @objc dynamic public var stock: Int = 0
    @objc dynamic public var descriptionProduct: String = ""
    @objc dynamic public var height: String = ""
    @objc dynamic public var width: String = ""
    @objc dynamic public var length: String = ""
    @objc dynamic public var taxClass: String = ""
    @objc dynamic public var shortDescription: String = ""
    
    override public static func primaryKey() -> String? {
        return "identifier"
    }
    public convenience init(title: String) {
        self.init()
        self.title = title
    }
}
