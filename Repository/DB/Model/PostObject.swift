//
//  PostObject.swift
//  Repository
//
//  Created by Fernando Jarilla on 8/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import RealmSwift

public final class PostObject: Object {
    @objc dynamic public var identifier: Int64 = 0
    @objc dynamic public var title: String = ""
    @objc dynamic public var publishedOn: String = ""
    @objc dynamic public var input: String = ""
    @objc dynamic public var writer: String = ""
    @objc dynamic public var publishedIn: String = ""
    @objc dynamic public var comments: String = ""
    @objc dynamic public var photo: String = ""
    
    override public static func primaryKey() -> String? {
        return "identifier"
    }
    
    public convenience init(title: String) {
        self.init()
        self.title = title
    }
}
