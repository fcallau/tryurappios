//
//  DBPersitable.swift
//  Repository
//
//  Created by Henry Bravo on 3/23/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import RealmSwift

public protocol DBPersistable {
    associatedtype ManagedObject: RealmSwift.Object
    associatedtype Query: QueryType
    
    init(managedObejct: ManagedObject)
    
    func managedObject() -> ManagedObject
}

// Protocol to execute queries
public protocol QueryType {
    var predicate: NSPredicate? { get }
    var sortDescriptors: [SortDescriptor] { get }
    
}
