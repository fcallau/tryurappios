//
//  Error.swift
//  Repository
//
//  Created by Henry Bravo on 3/31/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public enum DBError: Error {
    case emptyDatabase
}

extension DBError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .emptyDatabase:
            return NSLocalizedString("No data on disk", comment: "EmptyDatabaseError")
        }
    }
}
