//
//  RepositoryImpl.swift
//  Repository
//
//  Created by Henry Bravo on 3/31/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import RxSwift

public final class RepositoryImpl: Repository {
    public let cache: CacheRealmImpl
    let disposeBag = DisposeBag()
    
    public init() {
        cache = CacheRealmImpl()
    }
    
    public func getAllProducts(success: @escaping ProductObjectSuccessClosure, error: ErrorClosure?) {
        cache.getProducts(successClosure: { (products) in
            if (products.count > 0) {
                success(products)
            } else {
                self.populateProducts(success: success, errorClosure: error)
            }
            
        }) { _ in
            populateProducts(success: success, errorClosure: error)
        }
    }
    
    public func getAllPosts(success: @escaping PostObjectSuccessClosure, error: ErrorClosure?) {
        cache.getPosts(successClosure: { (posts) in
            if (posts.count > 0) {
                success(posts)
            } else {
                self.populatePosts(success: success, errorClosure: error)
            }
            
        }) { _ in
            populatePosts(success: success, errorClosure: error)
        }
    }
    
    public func populateProducts(success: @escaping ProductObjectSuccessClosure, errorClosure: ErrorClosure?) {
        let webService = WebService(configuration: .default)
        
        _ = webService
            .load([ProductWP].self, from: .allProducts)
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: { (products) in
                self.cache.writeProducts(products: products)
                self.cache.getProducts(successClosure: { (items) in
                    success(items)
                }, errorClosure: { (error) in
                    errorClosure!(error)
                })
            },
                onError: { (error) in
                errorClosure!(error)
            })
            .disposed(by: disposeBag)
    }
    
    public func populatePosts(success: @escaping PostObjectSuccessClosure, errorClosure: ErrorClosure?) {
        let webService = WebService(configuration: .default)
        
        _ = webService
            .load([PostWP].self, from: .allPosts)
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: { (posts) in
                    self.cache.writePosts(posts: posts)
                    self.cache.getPosts(successClosure: { (items) in
                        success(items)
                    }, errorClosure: { (error) in
                        errorClosure!(error)
                    })
            },
                onError: { (error) in
                    errorClosure!(error)
            })
            .disposed(by: disposeBag)
    }
}
