//
//  CacheRealmImpl.swift
//  Repository
//
//  Created by Henry Bravo on 3/29/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

import RealmSwift
import Realm
import RxSwift

public final class CacheRealmImpl: Cache {
    public let container = try! Container()
    
    public init() {}
    
    public func getProducts(successClosure: @escaping ProductObjectSuccessClosure, errorClosure: ErrorClosure) {
        if let fetchedResults = container.getValue(ProductWP.self, matching: .all) {
            successClosure(fetchedResults.results)
        } else {
            let error = DBError.emptyDatabase
            errorClosure(error)
        }
        
    }
    
    public func getPosts(successClosure: @escaping PostObjectSuccessClosure, errorClosure: ErrorClosure) {
        if let fetchedResults = container.getValue(PostWP.self, matching: .all) {
            successClosure(fetchedResults.results)
        } else {
            let error = DBError.emptyDatabase
            errorClosure(error)
        }
        
    }
    
    public func writeProducts(products: [ProductWP]) {
        try! container.write { (transaction) in
            transaction.addAll(products, update: true)
        }
    }
    
    public func writePosts(posts: [PostWP]) {
        try! container.write { (transaction) in
            transaction.addAll(posts, update: true)
        }
    }
}

